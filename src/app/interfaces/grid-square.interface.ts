export interface GridSquareInterface {
  user: string;
  status: 'sea' | 'ship' | 'shipEnvironment';
  hover: boolean;
  x: number;
  y: number;
}

export interface SquareInterfaceForServer {
  status: 'sea' | 'ship' | 'shipEnvironment',
  x: number;
  y: number;
}
