import { SquareInterfaceForServer } from './grid-square.interface';

export interface IloginInterface {
    status: string;
    data: IdataInterface;
}

interface IdataInterface {
    id?: string;
    name?: string;
    email?: string;
    grid?: SquareInterfaceForServer[][];
}

