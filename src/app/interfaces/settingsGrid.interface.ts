export interface settingsGridInterface {
    length: number,
    markup: {
        x: string,
        y: string
    }
}