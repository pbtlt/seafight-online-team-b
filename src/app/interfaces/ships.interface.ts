import { shipInterface } from './ship.interface';

export interface shipsInterface {
    battleship: shipInterface,
    cruiser: shipInterface,
    destroyer: shipInterface,
    torpedoBoat: shipInterface
}