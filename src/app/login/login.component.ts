import { WebsocketService } from './../services/websocket.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  private MOCK_SERVER_ID = '8nfg9829dnm209gfmn34';
  authForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern(/^([A-ZА-ЯЁЁ][a-zа-яёё]+)+$/)]),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(private loginService: LoginService, private websocketService: WebsocketService) { }

  getErrMsgNick() {
    return this.authForm.controls.name.hasError('required') ? 'You must enter a value' :
      this.authForm.controls.name.hasError('minlength') ? 'Minimum length 2 characters' :
        this.authForm.controls.name.hasError('pattern') ? 'Name must begin with a capital letter, and only letters' : '';
  }

  getErrMsgEmail() {
    return this.authForm.controls.email.hasError('required') ? 'You must enter a value' :
      this.authForm.controls.email.hasError('email') ? 'Not a valid email' : '';
  }

  authenticate() {
    this.loginService.setName(this.authForm.controls.name.value);
    this.loginService.setEmail(this.authForm.controls.email.value);
    console.log('authenticate')
    this.loginService.connectServer();
  }
}
