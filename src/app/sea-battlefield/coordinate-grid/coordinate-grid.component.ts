import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ChangeGridService } from "../../services/change-grid.service";
import { GridSquareInterface } from "../../interfaces/grid-square.interface";
import { ConfigService } from "../../services/config.service";
import { ChangeShipsToolsService } from 'src/app/services/change-ships-tools.service';
import { shipInterface } from 'src/app/interfaces/ship.interface';
import { interval } from 'rxjs';

@Component({
	selector: 'app-coordinate-grid',
	templateUrl: './coordinate-grid.component.html',
	styleUrls: ['./coordinate-grid.component.scss']
})
export class CoordinateGridComponent implements OnInit {
	@Input() user: string
	@Input() userStatus: string

	constructor(
		private chShTlsSvc: ChangeShipsToolsService,
		private changeGridSvc: ChangeGridService,
		private cfgSvc: ConfigService,
	){}

	isClear: boolean
	markupX: string
	markupY: string
	lengthGrid: number
	interval
	coordinates: GridSquareInterface [][]
	ngOnInit(){
		this.cfgSvc.getGridSettings().subscribe(settings => {
			this.markupX = settings.markup.x
			this.markupY = settings.markup.y
			this.lengthGrid = settings.length
			this.coordinates = this.changeGridSvc.generateCoordinates(this.lengthGrid, this.user)
		})

		const syncSecCountShips = interval(500)
		syncSecCountShips.subscribe(() => {
			if(this.userStatus == 'expectation') {
				this.isClear = false
			}
		})
	}

	selectedShip: shipInterface
	hoverSquare(square){
		this.selectedShip = this.chShTlsSvc.getShip()
		if(!this.selectedShip) return
		this.changeSquaresByShipLength(this.selectedShip, square)
	}

	crntCrdt: { x: number, y: number } // currentCoordinate
	changeSquaresByShipLength(ship, square){
		let x = square.x
		let y = square.y
		let hover = square.hover
		this.crntCrdt = { x: square.x, y: square.y }
		if(this.shipPosition) for(let i = 0; i < ship.length; i++) {
			try { this.coordinates[x][y + i].hover = hover }
			catch(e){}
		} else for(let i = 0; i < ship.length; i++) {
			try { this.coordinates[x + i][y].hover = hover }
			catch(e){}
		}
	}

	shipPosition: boolean = true
	@HostListener('window:keyup', ['$event'])
	onKeydown(event){
		this.selectedShip = this.chShTlsSvc.getShip()
		if(!this.selectedShip) return
		if(event.keyCode === 32) this.shipPosition = !this.shipPosition
		for(let i = 1; i < this.selectedShip.length; i++) {
			try {
				this.coordinates[this.crntCrdt.x][this.crntCrdt.y + i].hover = this.shipPosition
				this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y].hover = !this.shipPosition
			} catch (e) {}
		}
	}

	putShip(){
		if(!this.isClear) this.isClear = this.chShTlsSvc.isClear
		if(!this.selectedShip) return
		if(!this.selectedShip.qt) return
		if(this.shipPosition) this.putShipHorizontal()
		else this.putShipVertical()
		this.changeGridSvc.saveCoordinateGrid(this.coordinates);
	}

	clearShips(){
		this.ngOnInit()
		this.chShTlsSvc.resetShips()
	}

	private putShipVertical(){
		try { this.checkSquaresByVertical() }
		catch(err){ return this.setErrorMsg(err) }
		for(let i = 0; i < this.selectedShip.length; i++) {
			this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y].status = 'ship'
		}
		for(let i = -1; i <= 1; i++) {
			try{ this.coordinates[this.crntCrdt.x + this.selectedShip.length][this.crntCrdt.y + i].status = 'shipEnvironment' }
			catch(e){}
		}
		for(let i = -1; i <= 1; i++) {
			try{ this.coordinates[this.crntCrdt.x-1][this.crntCrdt.y + i].status = 'shipEnvironment' }
			catch(e){}
		}
		for(let i = 0; i < this.selectedShip.length; i++) {
			try{ this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y - 1].status = 'shipEnvironment' }
			catch(e){}
		}
		for(let i = 0; i < this.selectedShip.length; i++) {
			try{ this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y + 1].status = 'shipEnvironment' }
			catch(e){}
		}
		this.chShTlsSvc.subtractShip(this.selectedShip.type)
	}

	checkSquaresByVertical(){
		let status
		for(let i = 0; i < this.selectedShip.length; i++) {
			try{ status = this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y].status }
			catch(err){ throw new Error('You can’t put the ship out of the field!') }
			if(status === 'shipEnvironment') throw new Error('You can’t put the ships so close!')
			else if(status === 'ship') throw new Error('You can’t put on another ship!')
		}
	}

	private putShipHorizontal(){
		try{ this.checkSquaresByHorizontal() }
		catch(err){ return this.setErrorMsg(err) }
		for(let i = 0; i < this.selectedShip.length; i++) {
		 	this.coordinates[this.crntCrdt.x][this.crntCrdt.y + i].status = 'ship'
		}
		for(let i = -1; i <= 1; i++) {
			try{ this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y + this.selectedShip.length].status = 'shipEnvironment' }
			catch(e){}
		}
		for(let i = -1; i <= 1; i++) {
			try{ this.coordinates[this.crntCrdt.x + i][this.crntCrdt.y-1].status = 'shipEnvironment' }
			catch(e){}
		}
		for(let i = 0; i < this.selectedShip.length; i++) {
			try { this.coordinates[this.crntCrdt.x - 1][this.crntCrdt.y + i].status = 'shipEnvironment' }
			catch (e) {}
		}
		for(let i = 0; i < this.selectedShip.length; i++) {
			try { this.coordinates[this.crntCrdt.x + 1][this.crntCrdt.y + i].status = 'shipEnvironment' }
			catch (e) {}
		}
		this.chShTlsSvc.subtractShip(this.selectedShip.type)
	}

	checkSquaresByHorizontal(){
		let status
		for(let i = 0; i < this.selectedShip.length; i++) {
			try{ status = this.coordinates[this.crntCrdt.x][this.crntCrdt.y + i].status }
			catch(err){ throw new Error('You can’t put the ship out of the field!') }
			if(status === 'shipEnvironment') throw new Error('You can’t put the ships so close!')
			else if(status === 'ship') throw new Error('You can’t put on another ship!')
		}
	}

	isError: boolean
	errorMsg: string
	setErrorMsg(msg: string){
		this.isError = true
		setTimeout(() => this.isError = false, 1000)
		this.errorMsg = msg
	}
}
