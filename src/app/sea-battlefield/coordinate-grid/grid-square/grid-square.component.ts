import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GridSquareInterface } from "../../../interfaces/grid-square.interface";

@Component({
  selector: 'app-grid-square',
  templateUrl: './grid-square.component.html',
  styleUrls: ['./grid-square.component.scss']
})
export class GridSquareComponent implements OnInit {
  @Input() userStatus: string
  @Input() squareData: GridSquareInterface
  @Output() onClickSquare: EventEmitter<any> = new EventEmitter<any>()
  @Output() onMouseenterSquare: EventEmitter<any> = new EventEmitter<any>()

  ngOnInit() {
    this.squareData
  }

  clickSquare() {
    if (this.userStatus !== 'shipPlacement') return
    this.onClickSquare.emit(this.squareData);
  }

  hoverSquare(hover: boolean) {
    if (this.userStatus !== 'shipPlacement') return
    this.squareData.hover = hover;
    this.onMouseenterSquare.emit(this.squareData);
  }
}
