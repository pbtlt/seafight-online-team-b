import { Component } from '@angular/core';
import { timer } from "rxjs";
import { TipsService } from "../../services/tips.service";

@Component({
	selector: 'app-game-tips',
	templateUrl: './game-tips.component.html',
	styleUrls: ['./game-tips.component.scss']
})
export class GameTipsComponent {
	private number: number;
	public header: string;
	public text: string;

	constructor(private tipsSvc: TipsService){}

	ngOnInit() {
		timer(0, 5000).subscribe(() => {
				this.tipsSvc.getTips().subscribe(helpers => {
				this.number = Math.floor(Math.random() * Math.floor(helpers.length));
				return this.header = helpers[this.number].header, this.text = helpers[this.number].text;
			});
		});
	}

}
