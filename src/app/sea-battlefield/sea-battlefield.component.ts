import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { WebsocketService } from '../services/websocket.service';
import { ChangeGridService } from '../services/change-grid.service';

@Component({
	selector: 'app-sea-battlefield',
	templateUrl: './sea-battlefield.component.html',
	styleUrls: ['./sea-battlefield.component.scss']
})
export class SeaBattlefieldComponent {
	public player: string
	public enemy: string = 'Player'
	playerStatus: 'shipPlacement' | 'expectation' | 'start' = 'shipPlacement'
	enemyStatus: 'expectation' = 'expectation'
	visibilityTools: boolean

	constructor(
		private router: Router,
		private loginService: LoginService,
		private wss: WebsocketService,
		private gridSvc: ChangeGridService) {
		this.player = this.loginService.getName();
		if(!this.player) {
			this.router.navigate(['./login']);
		}
	}

	hiddenShipsTools(){
		this.visibilityTools = !this.visibilityTools
		const userId = this.loginService.getId()
		const grid = this.gridSvc.getCoordinateGrid()
		this.wss.sendMessageToServer(JSON.stringify({
			status: 'readyToPlay',
			data: {
				id: userId,
				grid: grid
			}
		}))
		// this.wss.getSocket().onmessage = data => {
		// 	console.log('on-click-ready-res:', data)
		// 	this.playerStatus = data.status
		// }
	}

}