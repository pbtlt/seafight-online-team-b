import { WebsocketService } from './../../services/websocket.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ChangeShipsToolsService } from 'src/app/services/change-ships-tools.service';
import { interval } from 'rxjs';
import { shipInterface } from 'src/app/interfaces/ship.interface';
import { shipsInterface } from 'src/app/interfaces/ships.interface';

@Component({
	selector: 'app-ships',
	templateUrl: './ships.component.html',
	styleUrls: ['./ships.component.scss']
})
export class ShipsComponent implements OnInit {
	@Output() onStartGame: EventEmitter<any> = new EventEmitter<any>()
	selectedShip: shipInterface
	shipsConfigs: shipsInterface
	readyBtn: boolean = true
	isTips: boolean = true
	timer: number = 90
	statusGame: boolean = false
	interval

	constructor(private selectedShipService: ChangeShipsToolsService, private ws: WebsocketService) { }

	ngOnInit() {
		const syncSecCountShips = interval(500)
		syncSecCountShips.subscribe(() => {
			this.selectedShipService.getDataShips()
				.subscribe(ships => {
					this.shipsConfigs = ships
					this.checkCountShips(ships)
				})
		})
	}

	checkCountShips(ships) {
		let countShips = ships.battleship.qt + ships.cruiser.qt + ships.destroyer.qt + ships.torpedoBoat.qt
		if (!countShips) this.readyBtn = false
	}

	selectShip(ship) {
		if (this.isTips) { setTimeout(() => this.isTips = false, 3500) }
		this.startTimer()
		if (!this.statusGame) this.statusGame = true
		this.selectedShip = ship
		this.selectedShipService.setShip(this.selectedShip)
	}

	startGame() {
		this.onStartGame.emit()
		this.ws.addUserToQueue()
		this.selectedShipService.isClear = false
	}

	stopGame() {
		console.log('Stop Game')
	}

	startTimer() {
		if (!this.statusGame) {
			this.interval = setInterval(() => {
				if (this.timer > 0) {
					this.timer--;
				} else {
					this.stopGame()
				}
			}, 1000)
		}
	}
}
