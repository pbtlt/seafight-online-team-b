import { Injectable } from '@angular/core';
import { SquareInterfaceForServer, GridSquareInterface } from "../interfaces/grid-square.interface";

@Injectable({
  providedIn: 'root'
})
export class ChangeGridService {
  coordinate: GridSquareInterface[][];
  generateCoordinates(length: number, user: string): GridSquareInterface[][] {
    return new Array(length)
      .fill(null, 0, length)
      .map((el, idxX) => new Array(length)
        .fill(null, 0, length)
        .map((el, idxY) => {
          return {
            user,
            status: 'sea',
            hover: false,
            x: idxX,
            y: idxY,
          };
        })
      );
  }
  saveCoordinateGrid(data: GridSquareInterface[][]) {
    this.coordinate = data;
  }
  prepareCoordinatesToServer() {
    const dataToChange = this.coordinate;
    const gridSquarePosition: SquareInterfaceForServer[][] = dataToChange.map(item => {
      const objX = item.map(el => {
        const objY = {
          status: el.status,
          x: el.x,
          y: el.y
        };
        return objY;
      });
      return objX;
    });
    return gridSquarePosition;
  }

  getCoordinateGrid() {
    return this.coordinate;
  }
}
