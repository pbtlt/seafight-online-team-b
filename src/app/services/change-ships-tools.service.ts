import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ConfigService } from "./config.service";
import { shipInterface } from '../interfaces/ship.interface';

@Injectable({
  providedIn: 'root'
})

export class ChangeShipsToolsService {
	selectedShip: shipInterface
	numShipsInstalled: { [k: string]: number } = {
		battleship: 0,
		cruiser: 0,
		destroyer: 0,
		torpedoBoat: 0
	}
	isClear: boolean = true

	constructor(private cfgSvc: ConfigService){}

  getDataShips() {
    return this.cfgSvc.getShips().pipe(
      map(ships => {
        return {
          battleship: {
            ...ships.battleship,
            qt: ships.battleship.qt -= this.numShipsInstalled.battleship
          },
          cruiser: {
            ...ships.cruiser,
            qt: ships.cruiser.qt -= this.numShipsInstalled.cruiser
          },
          destroyer: {
            ...ships.destroyer,
            qt: ships.destroyer.qt -= this.numShipsInstalled.destroyer
          },
          torpedoBoat: {
            ...ships.torpedoBoat,
            qt: ships.torpedoBoat.qt -= this.numShipsInstalled.torpedoBoat
          },
        }
      })
    )
  }

  setShip(ship: shipInterface) {
    this.selectedShip = ship
  }

  getShip() {
    return this.selectedShip
  }

	subtractShip(type){
		this.numShipsInstalled[type] += 1
		this.selectedShip.qt -= 1
	}

	resetShips(){
		this.numShipsInstalled = {
			battleship: 0,
			cruiser: 0,
			destroyer: 0,
			torpedoBoat: 0
		}
	}
}
