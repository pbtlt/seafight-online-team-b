import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { settingsGridInterface } from '../interfaces/settingsGrid.interface';
import { shipsInterface } from '../interfaces/ships.interface';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  gridSettingsUrl: string = 'assets/configs/grid-settings.json'
  shipsUrl: string =  'assets/configs/ships.json'

  constructor(private http: HttpClient){}

  getGridSettings(){
    return this.http.get<settingsGridInterface>(this.gridSettingsUrl)
  }

  getShips(){
    return this.http.get<shipsInterface>(this.shipsUrl)
  }
}
