import { WebsocketService } from './websocket.service';
import { Injectable, OnInit } from '@angular/core';
import { PersistanceService } from './persistance.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public user_id: string;
  private name: string;
  private email: string;
  constructor(private persister: PersistanceService, private router: Router, private wss: WebsocketService) {
    // this.wss.getSocket().onmessage = (event) => {
    //   console.log(event);
    //   const dataJson = JSON.parse(event.data);
    //   if (dataJson.id) {
    //     this.persister.set('ID', event.data);
    //     this.router.navigate(['./sea-battlefield']);
    //   } else {
    //     this.wss.setMessage(dataJson.message);
    //   }
    // };
  }

  setName(name: string) {
    this.persister.set('NAME', name);
    this.name = name;
  }

  getName() {
    return this.name;
  }

  setEmail(email: string) {
    this.email = email;
  }

  getEmail() {
    return this.email;
  }

  setId(id){
    this.persister.set('USER_ID', id);
    this.user_id = id
  }

  getId(){
    return this.user_id
  }

  connectServer(): void {
    const request = {
      name: this.name,
      email: this.email
    };
    this.wss.sendMessageToServer('Login', request);
    this.wss.getSocket().onmessage = (event) => {
      const dataJson = JSON.parse(event.data);
      if (dataJson.id) {
        this.persister.set('ID', dataJson.id);
        this.router.navigate(['./sea-battlefield']);
      }
    };
  }
}
