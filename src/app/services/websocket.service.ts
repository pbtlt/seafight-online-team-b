import { Injectable } from '@angular/core';
import { IloginInterface } from '../interfaces/serverInterfaces';
import { PersistanceService } from './persistance.service';
import { Router } from '@angular/router';
import { ChangeGridService } from './change-grid.service';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private socket
  private request: IloginInterface;
  public info: string;
  private message: string;
  public connection: boolean
  constructor(private persister: PersistanceService, private gridService: ChangeGridService) {
    // this.socket = new WebSocket('ws://localhost:3200');
    // this.socket.onclose = (event) => {
    //   console.error('Код: ' + event.code + ' причина: ' + event.reason);
    // };
    this.connect()
  }

  connect(){
    this.socket = new WebSocket('ws://localhost:3200');
    this.socket.onopen = () => {
      console.log('Соединение установлено.')
      this.connection = true
    }
    this.socket.onclose = () => {
      this.connection = false
      setTimeout(()=>{ this.connect() }, 1000)
    }
  }

  getSocket() {
    return this.socket;
  }

  setMessage(message: string) {
    this.message = message;
  }

  getMessage() {
    return this.message;
  }

  sendMessageToServer(status: string, data?) {
    this.request = {
      status,
      data,
    };
    const requestJson = JSON.stringify(this.request);
    this.socket.send(requestJson);
  }

  addUserToQueue() {
    this.request = {
      status: 'readyToPlay',
      data: {
        id: this.persister.get('ID'),
        grid: this.gridService.prepareCoordinatesToServer()
      }
    };
    const requestJson = JSON.stringify(this.request);
    this.socket.send(requestJson);
  }
}





